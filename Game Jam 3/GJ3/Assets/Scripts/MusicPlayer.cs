﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

	public GameObject menuMusic;
	public GameObject gameMusic;
	public float musicSpeed = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChangeMusic(int whichWay){
		//Stop All
		if (whichWay == 0) {
			menuMusic.GetComponent<AudioSource> ().Stop();
			gameMusic.GetComponent<AudioSource> ().Stop();
		}

		//Menu
		if (whichWay == 1) {
			menuMusic.GetComponent<AudioSource> ().Play();
		}

		//Gameplay
		if (whichWay == 2) {
			gameMusic.GetComponent<AudioSource> ().Play();
			gameMusic.GetComponent<AudioSource> ().pitch = 1 + 0.02f*(GetComponent<LevelNumber>().levelNumber - 1);
		}
	}

}
