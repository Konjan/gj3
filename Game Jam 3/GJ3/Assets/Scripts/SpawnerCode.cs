﻿using UnityEngine;
using System.Collections;

public class SpawnerCode : MonoBehaviour {

	private GameObject UniqueEntity;
	private AIMovement UniqueMovement;
	public GameObject[] PrefabList;
	public GameObject CheckEntity;
	private AIMovement CheckMovement;

	public int m_Types = 0;
	public int m_Differences = 0;

	private bool GameStart = false;
	private bool DifCheck = false;
	private int iterations = 0;

	public int test = 0;
	public int test2 = 0;
	public int test3 = 0;


	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.W))
		{
			CreateEntities(4, 3);
		}
	}

	public void CreateEntities(int Types, int Differences)
	{
		int Typ = Types;
		int Dif = Differences;

		if(Dif > 3)
			Dif = 3;

		for(int i = 0; i < Types; i++)
		{
			int prefabChoice = (int)Random.Range(0, 48);

			CheckEntity = PrefabList[prefabChoice];
			
			if(iterations == 0)
			{
				UniqueEntity = CheckEntity;
				UniqueMovement = CheckEntity.GetComponent<AIMovement>();
				UniqueMovement.isUnique = true;

				Instantiate(CheckEntity);
			}
			else if(iterations > 0)
			{
				CheckMovement = CheckEntity.GetComponent<AIMovement>();

				while(CheckEntity.name == UniqueEntity.name || DifCheck == false)
				{
					IterateRetry();
					
					if (Differences == CheckString())
						DifCheck = true;
				}

				for(int j = 0; j < (Random.Range(0, 6) + 1) + Typ; j++)
				{
					Instantiate(CheckEntity);
				}
			}

			iterations++;
		}
	}

	private void IterateRetry()
	{
		int prefabChoice = (int)Random.Range(0, 48);

		CheckEntity = PrefabList[prefabChoice];

		CheckMovement = CheckEntity.GetComponent<AIMovement>();
	}

	public void SetType(int Type)
	{
		m_Types = Type;
	}

	public void SetDifferences(int Differences)
	{
		m_Differences = Differences;
	}

	public void SetTypDif(int Type, int Dif)
	{
		m_Types = Type;
		m_Differences = Dif;
	}

	public int CheckString()
	{
		int tempVariable = 0;

		char[] CheckArray = CheckEntity.name.ToCharArray();
		char[] UniqueArray = UniqueEntity.name.ToCharArray();
		for(int i = 0; i < 3; i++)
		{
			if (CheckArray[i] == UniqueArray[i])
				tempVariable += 1;
		}

		return tempVariable;
	}

	public int CheckString(string Uni, string Check)
	{
		int tempVariable = 0;

		char[] CheckArray = Check.ToCharArray();
		char[] UniqueArray = Uni.ToCharArray();
		for (int i = 0; i < 3; i++)
		{
			if (CheckArray[i] == UniqueArray[i])
				tempVariable += 1;
		}

		return tempVariable;
	}
}
