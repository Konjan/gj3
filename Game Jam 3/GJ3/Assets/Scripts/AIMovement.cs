﻿using UnityEngine;
using System.Collections;

public enum WinDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
}


public class AIMovement : MonoBehaviour {
	//-----Function to determine whether the entity will continue to function or not-----//
	private bool m_bAlive = true;
	[HideInInspector]
	public bool isUnique = false;
	public WinDirection WinDir;

	public string ID;

	//-----Waypoint variable for seek function-----//
	public Vector3 m_Waypoint;

	private float speed = 10.0f;
	public float rotationSpeed = 360.0f;

	//-----As this value increases, so does the duration at which it takes for an entity to lerp from each waypoint-----//
	public float duration = 8.0f;

	//-----Debug options-----//
	public bool RotationTrigger = true;
	public bool DurationEndTrigger = true;
	public int DurationEnd = 15;
	public bool LookatTrigger = true;
	//-----Time used in debugging-----//
	public float RestartTime = 10.0f;
	public float GameTime = 10;
	private float timeStart;

	public float DebugEndtimer = 5;
	[HideInInspector]
	public float EndTimer = 0;
	public bool GameTrigger = false;

	private Vector3 temp_Direction;
	private Quaternion temp_Rotation;

	// Use this for initialization
	void Start ()
	{
		//-----Start by creating a  beginning waypoint-----//
		m_Waypoint = Waypoint();
		//-----Then make the object look toward that created waypoint-----//
		if(RotationTrigger)
			transform.LookAt(m_Waypoint);

		ID = "000";
	}

	// Update is called once per frame
	void Update ()
	{
		//-----------------------//
		//-----Waypoint Code-----//
		//-----------------------//
		if(GameTime > 0)
		{
			if(Vector3.Magnitude(transform.position - m_Waypoint) < 0.5f)
			{
				m_Waypoint = Waypoint();
			}


			GameTime -= Time.deltaTime;
		}
		else if(GameTime <= 0 && m_bAlive == true)
		{
			//-----Create a random number between -2 and 2 to identify which-----//
			//-----direction that the entity will move after time is up----------//
			int randomNum = (int)Random.Range(-2, 3);
			if (randomNum == 0)
				randomNum = 1;

			//-----Set waypoint as specified end position-----//
			m_Waypoint = Waypoint(randomNum);

			//-----If DebugTrigger is active, set ending duration to specified value-----//
			if(DurationEndTrigger)
				duration = DurationEnd;

			//-----Set Alive as false, to ensure this does not run again-----// 
			m_bAlive = false;

			EndTimer = DebugEndtimer;
		}

		//-----------------------//
		//-----Movement Code-----//
		//-----------------------//
		if (Vector3.Magnitude(transform.position - m_Waypoint) > 0.5f)
		{
			if (RotationTrigger)
			{
				if (LookatTrigger)
					transform.LookAt(m_Waypoint);
				else
				{
					temp_Rotation = Quaternion.LookRotation(m_Waypoint - transform.position);
					transform.rotation = Quaternion.Slerp(transform.rotation, temp_Rotation, rotationSpeed * Time.deltaTime);
				}
			}
			//transform.position += transform.TransformDirection(Vector3.forward) * speed * Time.deltaTime;
			Vector3 temp = Vector3.Lerp(transform.position, m_Waypoint, (Time.time - timeStart) / duration);
			if (m_bAlive == false)
				temp.y = 1;

			transform.position = temp;
		}
		if(EndTimer > 0)
		{
			EndTimer -= Time.deltaTime;

			if(EndTimer < 0)
			{
				GameTrigger = true;

				if (!isUnique)
					Destroy(this.gameObject, 1);
			}
		}
	}
	//-----Determine x and z values within the rectangle of the screen, and create a waypoint-----//
	private Vector3 Waypoint()
	{
		Vector3 tempWaypoint = RandomPointRectangle();

		//-----Reset timestart for the movement type that utilises it-----//
		timeStart = Time.time;

		return tempWaypoint;
	}

	//-----Create random point of 4, determining which side of the screen it will move to-----//
	private Vector3 Waypoint(int direction)
	{
		Vector3 tempWaypoint = Vector3.zero;

		if (direction < -1 || direction >= 2)
			tempWaypoint.x = 20 * direction / 2;
		else if (direction == 1 || direction == -1)
			tempWaypoint.z = 15 * direction;

		if(isUnique)
		{
			if(direction == -2)
			{
				WinDir = WinDirection.LEFT;
			}
			else if(direction == 2)
			{
				WinDir = WinDirection.RIGHT;
			}
			else if (direction == -1)
			{
				WinDir = WinDirection.DOWN;
			}
			else
			{
				WinDir = WinDirection.UP;
			}
		}

		tempWaypoint.y = 1;
		return tempWaypoint;
	}

	//-----Reset entity position and time to a new debugvalue-----//
	public void Reset()
	{
		Vector3 restart = Waypoint();
		transform.position = restart;

		GameTime = RestartTime;
		GameTrigger = false;
	}

	public Vector3 RandomPointRectangle()
	{
		Vector3 tempWaypoint;

		tempWaypoint.x = Random.Range(-5, 5);
		tempWaypoint.z = Random.Range(-9, 9);
		tempWaypoint.y = 1;

		return tempWaypoint;
	}

	public Vector3 RandomPointCircle()
	{
		Vector3 tempWaypoint = Random.insideUnitSphere;
		tempWaypoint.y = 1;

		return tempWaypoint;
	}
}

