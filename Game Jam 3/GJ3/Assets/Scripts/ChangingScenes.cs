﻿using UnityEngine;
using System.Collections;

public class ChangingScenes : MonoBehaviour {

	public GameObject MainMenuCanvas;
	public GameObject TutorialCanvas;
	public GameObject PauseCanvas;
	public GameObject LevelWipeCanvas;
	public GameObject GameOverCanvas;

	public void ButtonClick(int whichButton){
		//Play
		if (whichButton == 0){
			SwitchOffUI ();
			GetComponent<LevelNumber>().levelNumber = 1;
			GetComponent<MusicPlayer> ().ChangeMusic (0);
			GetComponent<MusicPlayer> ().ChangeMusic (2);
		}

		//Tutorial
		if (whichButton == 1){
			SwitchOffUI ();
			TutorialCanvas.SetActive (true);
		}

		//Pause
		if (whichButton == 2){
			PauseCanvas.SetActive (true);
			Time.timeScale = 0;
		}

		//Resume
		if (whichButton == 3){
			SwitchOffUI ();
			Time.timeScale = 1;
		}

		//Main Menu
		if (whichButton == 4){
			GetComponent<LevelNumber>().levelNumber = 0;
			Time.timeScale = 1;
			SwitchOffUI ();
			MainMenuCanvas.SetActive (true);
		}
	}

	public void SwitchOffUI(){
		MainMenuCanvas.SetActive (false);
		TutorialCanvas.SetActive (false);
		PauseCanvas.SetActive (false);
		LevelWipeCanvas.SetActive (false);
		GameOverCanvas.SetActive (false);
	}

}
